{
	inputs.nixpkgs.url = "github:nixos/nixpkgs";
	inputs.flake-utils.url = "github:numtide/flake-utils";

	outputs = { nixpkgs, flake-utils, ... }:
		flake-utils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" "aarch64-darwin" "x86_64-darwin" ] (system:
			let
				pkgs = import nixpkgs { inherit system; };
			in
			{
				devShells.default = pkgs.mkShell {
					packages = [ 	#pkgs.awscli2
												#pkgs.google-cloud-sdk
												pkgs.terraform
												pkgs.terragrunt
												pkgs.jq
												pkgs.openssh ];
				};
			}
		);
}
