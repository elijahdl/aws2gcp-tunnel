dependency "keys" {
  config_path = find_in_parent_folders("keys")
}

inputs = {
  aws_key_name = dependency.keys.outputs.aws_key_name
  key_name     = dependency.keys.outputs.key_name
}

include {
  path = find_in_parent_folders()
}

