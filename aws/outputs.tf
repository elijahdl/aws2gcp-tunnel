output "aws_vpc_id" {
  value = aws_vpc.aws_vpc.id
}

output "aws_igw_id" {
  value = aws_internet_gateway.main_igw.id
}

output "aws_subnet_ids" {
  value = aws_subnet.aws_subnet.*.id
}

output "aws_route_table_ids" {
  value = [aws_default_route_table.main.id]
  description = "A list of all route tables that should receive routes from GCP"
}

output "aws_instance_public_ip" {
  value       = aws_eip.main.*.public_ip
  description = "Publically reachable IP of AWS Instance"
}

output "aws_instance_private_ip" {
  value       = aws_instance.workstation[0].private_ip
  description = "Privately reachable IP of AWS Instance; the GCP instance should be able to reach this address"
}

output "aws_username" {
  value = var.ec2_username
  description = "Username used to login to AWS instances"
}

output "aws_instance_ssh_command" {
  value = "ssh -i ${var.key_name} ${var.ec2_username}@${aws_eip.main[0].public_ip}"
}