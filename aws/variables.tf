variable "aws_vpc_network_ip" {
  default     = "10.100.0.0"
  description = "Supernet for AWS VPC"
  type        = string
}

variable "vpc_cidr" {
  description = "Suprnet cidr mask for VPCs"
  type        = number
}

variable "subnet_mask" {
  description = "Subnet mask for all subnets"
  type        = number
}

variable "aws_ami" {
  default     = "ami-0326c3fa94bfbae06"
  description = "The ami to use for AWS instances"
  type        = string
}

variable "aws_key_name" {
  description = "Name of key pair to use for EC2 instances"
  type        = string
}

variable "key_name" {
  description = "The public key used to ssh into AWS instances"
  type        = string
}

variable "ec2_username" {
  default     = "admin"
  description = "Username used to login to EC2 instances"
  type        = string
}