resource "aws_vpc" "aws_vpc" {
  cidr_block = "${var.aws_vpc_network_ip}/${var.vpc_cidr}"

  tags = {
    Name = "aws-vpc"
  }
}

resource "aws_subnet" "aws_subnet" {
  count             = 1
  vpc_id            = aws_vpc.aws_vpc.id
  cidr_block        = cidrsubnet("${var.aws_vpc_network_ip}/${var.vpc_cidr}", var.subnet_mask - var.vpc_cidr, count.index)
  availability_zone = "us-east-1a"

  tags = {
    Name = "aws-subnet-${count.index}"
  }
}

resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.aws_vpc.id

  tags = {
    Name = "Main IGW"
  }
}

resource "aws_default_route_table" "main" {
  default_route_table_id = aws_vpc.aws_vpc.main_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_igw.id
  }

  tags = {
    Name = "Main Default Route Table"
  }
}

resource "aws_security_group" "allow_mgmt" {
  name        = "allow_mgmt"
  description = "Allow ICMP and SSH inbound traffic"
  vpc_id      = aws_vpc.aws_vpc.id

  ingress {
    description = "ICMP from All"
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
    to_port     = -1
    from_port   = -1
  }

  ingress {
    description = "SSH from All"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    to_port     = 22
    from_port   = 22
  }

  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    to_port     = 0
    from_port   = 0
  }
}

resource "aws_instance" "workstation" {
  count                  = 1
  ami                    = var.aws_ami
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.aws_subnet[count.index].id
  vpc_security_group_ids = [aws_security_group.allow_mgmt.id]
  key_name               = var.aws_key_name
}

resource "aws_eip" "main" {
  count    = 1
  instance = aws_instance.workstation[count.index].id
  vpc      = true
}