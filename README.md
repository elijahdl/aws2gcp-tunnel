# Automatic AWS to GCP HA IPSEC Tunnel via Terraform Configuration

Sets up 4 redundant IPSEC tunnels between AWS and GCP VPCs

All commands must be run from the project root

## Prerequisites

### Authentication

AWS - [Use one of the methods listed in the `aws_provider` documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication-and-configuration); I personally use [the AWS CLI to update the shared credentials file](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

GCP - [Use the Google Cloud SDK to authenticate to GCP](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#configuring-the-provider)

### Software

The following software is required to deploy this infrastructure:
- [Terraform](https://www.terraform.io/downloads)
- [Terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/)
- [OpenSSH](https://www.openssh.com/portable.html) - `ssh-keygen` is needed to generate the key pair used for logging into the test instances

The following software is required to run `testConnectivity.sh`:
- [jq](https://stedolan.github.io/jq/download/) - needed to parse json output from `terraform show -json` so we can grab IPs, instance usernames, etc.

### Nix Flake

If you use [the Nix package manager](https://nixos.org/download.html) and have [Flakes enabled](https://nixos.wiki/wiki/Flakes), you can run `nix develop` to get an environment with access to the software in the previous section without any permanent installation. The AWS and GCP CLI tools can be uncommented in `flake.nix` as well.

## Deployment

- Run `./initialize.sh` to create an ssh key pair that will be used for logging into the test instances
- Run `terragrunt run-all apply` to deploy everything

The amount of time it takes to deploy the project is highly variable. On a good day, this only takes 5 minutes, but sometimes it can take up to 20 minutes.
This is mainly due to the [`aws_vpn_connection`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpn_connection) resources, which can take between 2 and 15 minutes.

## Testing

`./testConnectivity.sh` runs a basic ping test between the instances in the two VPCs. This script reads the Terraform states to determine the information required to ssh into each instancee and ping across the tunnel. Once the ping tests start, use `ctrl-c` to exit.

## Destruction

Run `terragrunt run-all destroy`