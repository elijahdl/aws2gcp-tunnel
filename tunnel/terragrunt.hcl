dependency "gcp" {
  config_path = find_in_parent_folders("gcp")
}

dependency "aws" {
  config_path = find_in_parent_folders("aws")
}

inputs = {
  gcp_vpc_name        = dependency.gcp.outputs.gcp_vpc_name
  gcp_vpc_id          = dependency.gcp.outputs.gcp_vpc_id
  aws_vpc_id          = dependency.aws.outputs.aws_vpc_id
  aws_igw_id          = dependency.aws.outputs.aws_igw_id
  aws_subnet_ids      = dependency.aws.outputs.aws_subnet_ids
  aws_route_table_ids = dependency.aws.outputs.aws_route_table_ids
}

include {
  path = find_in_parent_folders()
}