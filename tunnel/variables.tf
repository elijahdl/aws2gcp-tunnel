variable "gcp_vpc_name" {
  description = "Name of GCP VPC"
  type        = string
}

variable "gcp_vpc_id" {
  description = "ID of GCP VPC"
  type        = string
}

variable "aws_vpc_id" {
  description = "ID of AWS VPC"
  type        = string
}

variable "aws_igw_id" {
  description = "ID of AWS Internet Gateway"
  type        = string
}

variable "gcp_vpc_network_ip" {
  description = "Network address of GCP VPC"
  type        = string
}

variable "aws_vpc_network_ip" {
  description = "Network address of AWS VPC"
  type        = string
}

variable "aws_subnet_ids" {
  description = "List of subnet IDs to attach to the transit gateway"
  type        = list(string)
}

variable "aws_route_table_ids" {
  description = "A list of all aws route tables that should receive routes from GCP"
  type        = list(string)
}

variable "vpc_cidr" {
  description = "CIDR mask for VPCs"
  type        = number
}

variable "tunnels_networks_internal_ip" {
  description = "The network IP of a /28 network that conforms to https://docs.aws.amazon.com/vpn/latest/s2svpn/VPNTunnels.html"
  default     = "169.254.20.0"
  type        = string
}