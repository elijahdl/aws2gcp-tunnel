# Guidance taken from https://cloud.google.com/architecture/build-ha-vpn-connections-google-cloud-aws

resource "google_compute_ha_vpn_gateway" "gcp_vpn_gateway" {
  name    = "ha-vpn-gateway"
  network = var.gcp_vpc_id
}

resource "google_compute_router" "gcp_router" {
  name    = "gcp-router"
  network = var.gcp_vpc_name
  bgp {
    asn               = 65534
    advertise_mode    = "CUSTOM"
    advertised_groups = ["ALL_SUBNETS"]
  }
}

resource "aws_customer_gateway" "gcp_gw" {
  count      = 2
  bgp_asn    = google_compute_router.gcp_router.bgp[0].asn
  ip_address = google_compute_ha_vpn_gateway.gcp_vpn_gateway.vpn_interfaces[count.index].ip_address
  type       = "ipsec.1"

  tags = {
    Name = "gcp-gateway-${count.index}"
  }
}

resource "aws_ec2_transit_gateway" "gcp_transit_gateway" {
  description = "GCP Transit Gateway"

  amazon_side_asn = 65123
  
  auto_accept_shared_attachments = "enable"
  default_route_table_association = "enable"
  default_route_table_propagation = "enable"
  dns_support = "enable"
  vpn_ecmp_support = "enable"

  tags = {
    Name = "gcp-transit-gateway"
  }
}

resource "aws_ec2_transit_gateway_vpc_attachment" "gcp_tg_attachment" {
  subnet_ids          = var.aws_subnet_ids
  transit_gateway_id  = aws_ec2_transit_gateway.gcp_transit_gateway.id
  vpc_id              = var.aws_vpc_id

  tags = {
    Name = "gcp-transit-gateway-attachment"
  }
}

# We're going to let AWS generate preshared keys for us;
# we'll refer to them when we set up the tunnel on GCP
resource "aws_vpn_connection" "gcp_vpn_connection" {
  count               = 2
  transit_gateway_id  = aws_ec2_transit_gateway.gcp_transit_gateway.id
  customer_gateway_id = aws_customer_gateway.gcp_gw[count.index].id
  type                = aws_customer_gateway.gcp_gw[count.index].type

  tunnel1_inside_cidr = cidrsubnet("${var.tunnels_networks_internal_ip}/28", 2, (count.index * 2))
  tunnel2_inside_cidr = cidrsubnet("${var.tunnels_networks_internal_ip}/28", 2, (count.index * 2) + 1)

  tags = {
    Name = "gcp-vpn-connection-${count.index}"
  }
}


locals {
  # Create a list aws peer IPs
  aws_ips             = [ aws_vpn_connection.gcp_vpn_connection[0].tunnel1_address,
                          aws_vpn_connection.gcp_vpn_connection[0].tunnel2_address,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel1_address,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel2_address ]
  gcp_internal_gw_ips = [ aws_vpn_connection.gcp_vpn_connection[0].tunnel1_cgw_inside_address,
                          aws_vpn_connection.gcp_vpn_connection[0].tunnel2_cgw_inside_address,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel1_cgw_inside_address,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel2_cgw_inside_address ]
  aws_internal_gw_ips = [ cidrhost(aws_vpn_connection.gcp_vpn_connection[0].tunnel1_inside_cidr, 1),
                          cidrhost(aws_vpn_connection.gcp_vpn_connection[0].tunnel2_inside_cidr, 1),
                          cidrhost(aws_vpn_connection.gcp_vpn_connection[1].tunnel1_inside_cidr, 1),
                          cidrhost(aws_vpn_connection.gcp_vpn_connection[1].tunnel2_inside_cidr, 1) ]
  shared_secrets      = [ aws_vpn_connection.gcp_vpn_connection[0].tunnel1_preshared_key,
                          aws_vpn_connection.gcp_vpn_connection[0].tunnel2_preshared_key,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel1_preshared_key,
                          aws_vpn_connection.gcp_vpn_connection[1].tunnel2_preshared_key ]
}

resource "google_compute_external_vpn_gateway" "aws_gateway" {
  name            = "aws-gateway"
  redundancy_type = "FOUR_IPS_REDUNDANCY"
  description     = "AWS managed VPN gateway"

  # Creates 4 interfaces with respective peer IP configurations
  dynamic "interface" {
    for_each = range(4)
    content {
      id         = interface.value
      ip_address = local.aws_ips[interface.value]
    }
  }
}

resource "google_compute_vpn_tunnel" "tunnel" {
  count = 4
  name  = "aws-tunnel-${count.index}"

  shared_secret = local.shared_secrets[count.index]
  ike_version   = 2

  router                          = google_compute_router.gcp_router.id
  vpn_gateway                     = google_compute_ha_vpn_gateway.gcp_vpn_gateway.id
  peer_external_gateway           = google_compute_external_vpn_gateway.aws_gateway.id
  peer_external_gateway_interface = count.index
  vpn_gateway_interface           = floor(count.index / 2)
}

resource "google_compute_router_interface" "vpn_interface" {
  count = 4
  name  = "int-${count.index}"

  router     = google_compute_router.gcp_router.name
  ip_range   = "${local.gcp_internal_gw_ips[count.index]}/30"
  vpn_tunnel = google_compute_vpn_tunnel.tunnel[count.index].name
}

resource "google_compute_router_peer" "bgp_peer" {
  count           = 4
  name            = "aws-bgp-peer-${count.index}"
  
  router          = google_compute_router.gcp_router.name
  peer_ip_address = local.aws_internal_gw_ips[count.index]
  peer_asn        = aws_ec2_transit_gateway.gcp_transit_gateway.amazon_side_asn
  interface       = google_compute_router_interface.vpn_interface[count.index].name
}

data "aws_vpc" "aws_vpc" {
  id = var.aws_vpc_id
}

# resource "aws_default_route_table" "main" {
#   default_route_table_id = data.aws_vpc.aws_vpc.main_route_table_id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = var.aws_igw_id
#   }

#   route {
#     cidr_block = "${var.gcp_vpc_network_ip}/${var.vpc_cidr}"
#     transit_gateway_id = aws_ec2_transit_gateway.gcp_transit_gateway.id
#   }

#   tags = {
#     Name = "Main Default Route Table"
#   }
# }

resource "aws_route" "gcp_route" {
  for_each = toset(var.aws_route_table_ids)
  route_table_id            = each.key
  destination_cidr_block    = "${var.gcp_vpc_network_ip}/${var.vpc_cidr}"
  transit_gateway_id        = aws_ec2_transit_gateway.gcp_transit_gateway.id
}