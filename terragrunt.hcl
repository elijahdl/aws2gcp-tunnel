generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite"
  contents  = <<EOF
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.25.0"
    }
    google = {
      source  = "hashicorp/google-beta"
      version = "4.41.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "google" {
  region  = "us-east4"
  zone    = "us-east4-b"
  project = "terraform-c2c-tunnel"
}
EOF
}

inputs = {
  gcp_vpc_network_ip = "10.10.0.0"
  aws_vpc_network_ip = "10.200.0.0"
  vpc_cidr           = 16
  subnet_mask        = 23
  gcp_username       = "eli_dlove"
  ec2_username       = "admin"
  key_name           = "aws2gcp-test.rsa"
}