output "gcp_vpc_name" {
  value = google_compute_network.gcp_vpc.name
}

output "gcp_vpc_id" {
  value = google_compute_network.gcp_vpc.id
}

output "gcp_subnet_names" {
  value = google_compute_subnetwork.gcp_subnet.*.name
}

output "gcp_instance_public_ip" {
  value       = google_compute_instance.workstation[0].network_interface[0].access_config[0].nat_ip
  description = "Publically reachable IP of GCP Instance"
}

output "gcp_instance_private_ip" {
  value       = google_compute_instance.workstation[0].network_interface[0].network_ip
  description = "Privately reachable IP of GCP Instance; the AWS instance should be able to reach this address"
}

output "gcp_username" {
  value = var.gcp_username
  description = "Username used to login to GCP instances"
}

output "gcp_instance_ssh_command" {
  value = "ssh -i ${var.key_name} ${var.gcp_username}@${google_compute_instance.workstation[0].network_interface[0].access_config[0].nat_ip}"
  description = "Command to use to connect to the GCP instance"
}