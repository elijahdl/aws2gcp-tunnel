variable "gcp_vpc_network_ip" {
  default     = "10.50.0.0"
  description = "Supernet for GCP VPC"
  type        = string
}

variable "vpc_cidr" {
  description = "Suprnet cidr mask for VPCs"
  type        = number
}

variable "subnet_mask" {
  description = "Subnet mask for all subnets"
  type        = number
}

variable "gcp_image" {
  default     = "debian-cloud/debian-11"
  description = "The image to use for GCP instances"
  type        = string
}

variable "pubkey" {
  description = "The public key used to ssh into instances"
  type        = string
}

variable "key_name" {
  description = "The public key used to ssh into instances"
  type        = string
}

variable "gcp_username" {
  description = "Username used to login to instances"
  type        = string
}