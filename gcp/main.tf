resource "google_compute_network" "gcp_vpc" {
  name                    = "gcp-vpc"
  auto_create_subnetworks = false
  routing_mode            = "GLOBAL"
}

resource "google_compute_subnetwork" "gcp_subnet" {
  count         = 1
  name          = "gcp-subnet-${count.index}"
  network       = google_compute_network.gcp_vpc.id
  ip_cidr_range = cidrsubnet("${var.gcp_vpc_network_ip}/${var.vpc_cidr}", var.subnet_mask - var.vpc_cidr, count.index)
}

resource "google_compute_firewall" "gcp_fw" {
  name    = "gcp-firewall"
  network = google_compute_network.gcp_vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "workstation" {
  count = 1
  name  = "workstation-${count.index}"

  machine_type              = "e2-medium"
  allow_stopping_for_update = true

  tags = ["workstation"]

  boot_disk {
    initialize_params {
      image = var.gcp_image
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.gcp_subnet[count.index].name
    access_config {}
  }

  metadata = {
    ssh-keys = "${var.gcp_username}:${var.pubkey}"
  }

  metadata_startup_script = "sudo apt-get update; sudo apt-get install -y tcpdump"
}