dependency "keys" {
  config_path = find_in_parent_folders("keys")
}

inputs = {
  pubkey   = dependency.keys.outputs.pubkey
  key_name = dependency.keys.outputs.key_name
}

include {
  path = find_in_parent_folders()
}

