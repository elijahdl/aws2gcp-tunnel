#!/bin/bash

# Colors
RED='\033[0;31m'
GREEN='\033[;32m'
NC='\033[0m' # No Color

echo "Getting key pair..."
export PUBKEY=$(cd keys; terragrunt show -json | jq -r .values.outputs.key_name.value)

if [ -f $PUBKEY ]; then
    echo "Key pair ${PUBKEY} found!"
else
    printf "${RED}No public key found! Is the project deployed?${NC}\n"
    exit
fi

echo "Getting AWS state..."
export AWS_STATE=$(cd aws; terragrunt show -json | \
    jq '{aws_pub_ip:     .values.outputs.aws_instance_public_ip.value[0],
         aws_private_ip: .values.outputs.aws_instance_private_ip.value,
         aws_username:   .values.outputs.aws_username.value}')

export AWS_INSTANCE_PUBLIC_IP=$(echo $AWS_STATE | jq -r .aws_pub_ip)
export AWS_INSTANCE_PRIVATE_IP=$(echo $AWS_STATE | jq -r .aws_private_ip)
export AWS_USERNAME=$(echo $AWS_STATE | jq -r .aws_username)

if [ $AWS_INSTANCE_PRIVATE_IP = "null" ]; then
    printf "${RED}Error reading AWS instance state! Make sure that 'aws' is deployed!${NC}\n"
    exit
else
    echo "AWS instance state found!"
fi

echo "Getting GCP state..."
export GCP_STATE=$(cd gcp; terragrunt show -json | \
    jq '{gcp_pub_ip:     .values.outputs.gcp_instance_public_ip.value,
         gcp_private_ip: .values.outputs.gcp_instance_private_ip.value,
         gcp_username:   .values.outputs.gcp_username.value}')

export GCP_INSTANCE_PUBLIC_IP=$(echo $GCP_STATE | jq -r .gcp_pub_ip)
export GCP_INSTANCE_PRIVATE_IP=$(echo $GCP_STATE | jq -r .gcp_private_ip)
export GCP_USERNAME=$(echo $GCP_STATE | jq -r .gcp_username)

if [ $GCP_INSTANCE_PRIVATE_IP = "null" ]; then
    printf "${RED}Error reading GCP instance state! Make sure that 'gcp' is deployed!${NC}\n"
    exit
else
    echo "GCP instance state found!"
fi

# USAGE: check-connectivity $SSH_IP $SSH_USERNAME $PING_TARGET $SOURCE_HOST_NAME $DEST_HOST_NAME
check-connectivity () {
    if ssh -i $PUBKEY -oStrictHostKeyChecking=no "${2}@${1}" ping -c 1 -w 3 ${3} &> /dev/null
    then
        printf "$(date) - ${GREEN}SUCCESS! Can ping from ${4} instance to ${5} private IP${NC}\n" 
    else
        printf "$(date) - ${RED}FAILURE! Cannot ping from ${4} instance to ${5} private IP${NC}\n"
    fi
}

echo "Starting connectivity checks... ctrl-c to exit"

while true; do
    echo "$(date) - Checking connectivity..."
    check-connectivity  ${AWS_INSTANCE_PUBLIC_IP} \
                        ${AWS_USERNAME} \
                        ${GCP_INSTANCE_PRIVATE_IP} \
                        "AWS" "GCP"
    check-connectivity  ${GCP_INSTANCE_PUBLIC_IP} \
                        ${GCP_USERNAME} \
                        ${AWS_INSTANCE_PRIVATE_IP} \
                        "GCP" "AWS"
    sleep 10s
done
