variable "key_name" {
  description = "Name of key to be used to SSH into test instances"
  type        = string
}

data "local_file" "pubkey" {
  filename = "${path.module}/../${var.key_name}.pub"
}

resource "aws_key_pair" "aws_key" {
  key_name   = var.key_name
  public_key = data.local_file.pubkey.content
}

output "aws_key_name" {
  value = aws_key_pair.aws_key.key_name
}

output "pubkey" {
  value       = data.local_file.pubkey.content
  description = "The public key used to ssh into the instances"
  sensitive   = true
}

output "key_name" {
  value       = var.key_name
  description = "Name of key to be used to SSH into test instances"
}